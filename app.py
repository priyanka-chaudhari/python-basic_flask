from flask import Flask, jsonify, render_template, request

app = Flask(__name__)

#Sample JSON file for accessing data without db.
stores = [
    {
        'name' : "Dmart",
        'items' : [
            {
                'name'  : 'jim-jam',
                'price' :  25
            }     
        ]
    },
    {
        'name' : "super",
        'items' : [
               
        ]
    }
]

"""tells the application which URL should call the associated function."""
@app.route("/") 
def home():
    return render_template("index.html")


#GET  -  used to send data back only
@app.route("/stores")
def get_stores():
    return jsonify({'stores': stores})

#POST  - /store data: {name:}
@app.route("/store", methods=['POST'])
def create_new_store():
    request_name = request.get_json()

    new_store = {
        'name'  : request_name['name'],
        'items'  : []
    }
    stores.append(new_store)
    return jsonify(new_store)
# return jsonify({'message': 'updated store list is: '+stores})


# GET  - /stores/<string:name>
@app.route("/stores/<string:name>")
def get_store(name):
    for store in stores:
        if store['name'] == name:
            return jsonify(store)
    return jsonify({'mesage': 'store not found'})


# GET  - /stores/<string:name>/items
@app.route("/stores/<string:name>/item")
def get_items_in_store(name):
    for store in stores:
        if store['name'] == name:
            if store['items']:    
                return jsonify(store['items'])
            else:
                return jsonify({'message': name+ " store does not contains items"})
    return jsonify({'message': name+ " store does not exist"})


#POST  /store/<string:name>/item {name:, price:}
@app.route("/store/<string:name>/item", methods=['POST'])
def create_item_in_store(name):
    request_item = request.get_json()
    for store in stores:
        if store['name'] == name:
            new_item = {
                'name'  : request_item['name'],
                'price' : request_item['price']
            }

            store['items'].append(new_item)
            return (jsonify(new_item))

    return jsonify({'message': " {} does not exist." .format(name)})



# //method of Flask class
app.run(port=5003, debug=True) #//app.run(host, port, debug, options)

